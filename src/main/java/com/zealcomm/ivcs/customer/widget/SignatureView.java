package com.zealcomm.ivcs.customer.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class SignatureView extends WebView {

    private OnCanvasUpdateListener onCanvasUpdateListener;

    public SignatureView(@NonNull Context context) {
        this(context, null);
    }

    public SignatureView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SignatureView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getSettings().setAllowFileAccess(true);
        getSettings().setJavaScriptEnabled(true);
        loadUrl("file:///android_asset/signature/signature.html");
        addJavascriptInterface(this, "ivcsCustomer");
        setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
        });
    }

    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void updateCanvasJson(String canvasJson) {
        if (null != onCanvasUpdateListener) {
            onCanvasUpdateListener.onUpdate(canvasJson);
        }
    }

    public void clearCanvas() {
        loadUrl("javascript:clearCanvas()");
    }

    public interface OnCanvasUpdateListener {
        void onUpdate(String canvasJson);
    }

    public void setOnCanvasUpdateListener(OnCanvasUpdateListener onCanvasUpdateListener) {
        this.onCanvasUpdateListener = onCanvasUpdateListener;
    }
}
