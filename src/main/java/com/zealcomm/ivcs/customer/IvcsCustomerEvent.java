package com.zealcomm.ivcs.customer;

import com.zealcomm.base.entity.CustomMessage;
import com.zealcomm.ivcs.customer.base.entity.CollectedCustomerInfo;
import com.zealcomm.ivcs.customer.base.entity.FormData;
import com.zealcomm.ivcs.customer.base.entity.UserInfo;

public interface IvcsCustomerEvent {

    /**
     * when a new video stream joins or leave the room ，this event is triggered
     * @param videoLabel stream type
     * @param status true : joins,false : leave
     * */
    void onVideoLabel(String videoLabel,boolean status);
    /**
     * when receiving a resource link, this event is triggered
     * @param url resource url
     * @param type resource type(audio ,video,link,file,etc)
     * */
    void onResource(String url, String type,String name);
    /**
     * When a service is stopped, this event is triggered
     * @param reason reason for drop(user login in other device or service stopped,etc)
     * */
    void onDrop(String reason);
    /**
     * when receiving a text, this event is triggered
     * @param text text message from other user
     * */
    void onText(String text);
    /**
     * When an error occurs in the current service, this event is triggered
     * @param errorCode error code
     * */
    void onError(int errorCode);
    /**
     * if the request for form data is successful, this event is triggered
     * @param formListDataList form data for current groups
     * */
    void onFormData(FormData formListDataList);

    void onCollectInfo(CollectedCustomerInfo collectedCustomerInfo) ;
    /**
     * new user joined current session
     * @param userInfo joined user info
     * */
    void onUserJoined(UserInfo userInfo);
    /**
     * user exit current session
     * @param userInfo exit user info
     * */
    void onUserQuit(UserInfo userInfo);

    /**
     * Service starts, trigger this event
     * */
    void onReadyTalk();
    /**
     * After receiving a hand signature request, trigger this event
     * */
    void onHandSignature();
    /**
     * After receiving a guide frame request, trigger this event
     * */
    void onGuideBox();
    /**
     * Transfer service to others, trigger this event
     * */
    void onTransferring();
    /**
     * Progress callback for session request
     * */
    void onInvitationProgress(String description);
    /**
     * After receiving a screen snapshot request, trigger this event
     * */
    void onScreenSnapshot();
    /**
     * After receiving a screen share request, trigger this event
     * */
    void onViewScreen();
    /**
     * After receiving a tips of device operation, trigger this event
     * */
    void onMarkScreen(int x, int y);
    /**
     * After receiving a custom message, trigger this event
     * */
    void onCustomMessage(CustomMessage customMessage);
}
