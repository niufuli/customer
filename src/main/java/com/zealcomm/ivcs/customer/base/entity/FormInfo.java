package com.zealcomm.ivcs.customer.base.entity;

import java.util.HashMap;

public class FormInfo {

    /**
     * sessionId : SessionId
     * formTemplateId : formTemplateId
     * fields : [{"key1":"value1"}]
     */

    private String sessionId;
    private String formTemplateId;
    private HashMap<String,String> fields;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getFormTemplateId() {
        return formTemplateId;
    }

    public void setFormTemplateId(String formTemplateId) {
        this.formTemplateId = formTemplateId;
    }

    public HashMap<String,String> getFields() {
        return fields;
    }

    public void setFields(HashMap<String,String> fields) {
        this.fields = fields;
    }
}
