package com.zealcomm.ivcs.customer.base.entity;

public class TokenBody {


    /**
     * data : {"_id":"5e706ab0c503b07b62fa2a3d","userName":"ljk","userPassword":"e10adc3949ba59abbe56e057f20f883e","__v":0}
     * iat : 1584445131
     * exp : 1584488331
     */

    private DataBean data;
    private int iat;
    private int exp;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getIat() {
        return iat;
    }

    public void setIat(int iat) {
        this.iat = iat;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public static class DataBean {
        /**
         * _id : 5e706ab0c503b07b62fa2a3d
         * userName : ljk
         * userPassword : e10adc3949ba59abbe56e057f20f883e
         * __v : 0
         */

        private String _id;
        private String userName;
        private String userPassword;
        private int __v;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserPassword() {
            return userPassword;
        }

        public void setUserPassword(String userPassword) {
            this.userPassword = userPassword;
        }

        public int get__v() {
            return __v;
        }

        public void set__v(int __v) {
            this.__v = __v;
        }
    }
}
