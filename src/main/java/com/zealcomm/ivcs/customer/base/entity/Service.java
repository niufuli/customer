package com.zealcomm.ivcs.customer.base.entity;

public class Service {

    /**
     * _id : 5f16837d157ee80012075366
     * weight : 1
     * name : 查询服务
     * id : 5f16837d157ee80012075366
     */

    private String _id;
    private int weight;
    private String name;
    private String id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
