package com.zealcomm.ivcs.customer.base.https;

import com.zealcomm.base.entity.LoginReq;
import com.zealcomm.base.entity.RecordBody;
import com.zealcomm.ivcs.customer.base.entity.FormInfo;
import com.zealcomm.ivcs.customer.base.entity.IDCardAndBankInfo;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface CustomerApiDemo extends CustomerCommonApi {

    /**
     * 登录
     */
    @POST("/api/v1/auth/logon")
    Call<ResponseBody> login(@Body LoginReq tokenReq);

    /**
     * 提交图片
     */
    @Multipart
    @POST()
    Call<ResponseBody> uploadSignatureImg(@Header("x-access-token") String token,@Url String url, @Part MultipartBody.Part part);

    /**
     * get service groups
     */
    @GET("/api/groups/options")
    Call<ResponseBody> getServiceGroups(@Header("x-access-token") String token);

    @POST("/rooms/{roomId}/recordings")
    Call<ResponseBody> startRecord(@Header("x-access-token") String token, @Path("roomId") String roomId, @Body RecordBody recordBody);

    @POST("/rooms/{roomId}/recordings/{recordId}")
    Call<ResponseBody> stopRecord(@Header("x-access-token") String token, @Path("roomId") String roomId, @Path("recordId") String recordId);

    @POST("/api/v1/forminfo")
    Call<ResponseBody> submitIDCardAndBankInfo(@Header("x-access-token") String token, @Body IDCardAndBankInfo idCardAndBankInfo);

    /**
     * 提交用户信息
     */
    Call<ResponseBody> commitForm(@Header("x-access-token") String token, @Body FormInfo formInfo , @Path("sessionId") String sessionId);
}
