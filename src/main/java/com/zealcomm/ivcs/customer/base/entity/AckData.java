package com.zealcomm.ivcs.customer.base.entity;

public class AckData {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
