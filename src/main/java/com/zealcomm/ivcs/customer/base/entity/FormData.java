package com.zealcomm.ivcs.customer.base.entity;

import java.util.List;

public class FormData {
    /**
     * type : form
     * id : 942843577278134500
     * from : 100094893891504270
     * to : customers
     * data : {"inputform":1,"formTemplate":{"_id":"5f098bd7aff1c50012b6c71d","name":"查询表单","fields":[{"_id":"5f098bd7aff1c50012b6c721","name":"请输入你的银行卡号"},{"_id":"5f098bd7aff1c50012b6c720","name":"请输入办卡人姓名"},{"_id":"5f098bd7aff1c50012b6c71f","name":"请输入办卡人身份证号"},{"_id":"5f098bd7aff1c50012b6c71e","name":"请输入办卡人手机号"}],"__v":0}}
     */

    private String type;
    private String id;
    private String from;
    private String to;
    private DataBean data;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * inputform : 1
         * formTemplate : {"_id":"5f098bd7aff1c50012b6c71d","name":"查询表单","fields":[{"_id":"5f098bd7aff1c50012b6c721","name":"请输入你的银行卡号"},{"_id":"5f098bd7aff1c50012b6c720","name":"请输入办卡人姓名"},{"_id":"5f098bd7aff1c50012b6c71f","name":"请输入办卡人身份证号"},{"_id":"5f098bd7aff1c50012b6c71e","name":"请输入办卡人手机号"}],"__v":0}
         */

        private int inputform;
        private FormTemplateBean formTemplate;

        public int getInputform() {
            return inputform;
        }

        public void setInputform(int inputform) {
            this.inputform = inputform;
        }

        public FormTemplateBean getFormTemplate() {
            return formTemplate;
        }

        public void setFormTemplate(FormTemplateBean formTemplate) {
            this.formTemplate = formTemplate;
        }

        public static class FormTemplateBean {
            /**
             * _id : 5f098bd7aff1c50012b6c71d
             * name : 查询表单
             * fields : [{"_id":"5f098bd7aff1c50012b6c721","name":"请输入你的银行卡号"},{"_id":"5f098bd7aff1c50012b6c720","name":"请输入办卡人姓名"},{"_id":"5f098bd7aff1c50012b6c71f","name":"请输入办卡人身份证号"},{"_id":"5f098bd7aff1c50012b6c71e","name":"请输入办卡人手机号"}]
             * __v : 0
             */

            private String _id;
            private String name;
            private int __v;
            private List<FieldsBean> fields;

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int get__v() {
                return __v;
            }

            public void set__v(int __v) {
                this.__v = __v;
            }

            public List<FieldsBean> getFields() {
                return fields;
            }

            public void setFields(List<FieldsBean> fields) {
                this.fields = fields;
            }

            public static class FieldsBean {
                /**
                 * _id : 5f098bd7aff1c50012b6c721
                 * name : 请输入你的银行卡号
                 */

                private String _id;
                private String name;

                public String get_id() {
                    return _id;
                }

                public void set_id(String _id) {
                    this._id = _id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }
            }
        }
    }
}
