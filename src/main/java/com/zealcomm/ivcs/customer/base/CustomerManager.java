package com.zealcomm.ivcs.customer.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zealcomm.app.util.ExceptionUtil;
import com.zealcomm.app.util.GsonUtil;
import com.zealcomm.app.util.LogUtil;
import com.zealcomm.base.BaseManager;
import com.zealcomm.base.Constants;
import com.zealcomm.base.HttpErrorCode;
import com.zealcomm.base.ImageUtil;
import com.zealcomm.base.MessageData;
import com.zealcomm.base.ServerCallback;
import com.zealcomm.base.TLSSocketFactory;
import com.zealcomm.base.entity.CustomMessage;
import com.zealcomm.base.entity.CustomerLoginRes;
import com.zealcomm.base.entity.HandWritingDataWrapper;
import com.zealcomm.base.entity.LinkData;
import com.zealcomm.base.entity.LinkInfo;
import com.zealcomm.base.entity.LoginError;
import com.zealcomm.base.entity.MediaOptions;
import com.zealcomm.base.entity.Profile;
import com.zealcomm.base.entity.SessionData;
import com.zealcomm.base.entity.SessionMessage;
import com.zealcomm.base.entity.StatusMsg;
import com.zealcomm.base.entity.UploadImageRes;
import com.zealcomm.ccs.CcsClient;
import com.zealcomm.ccs.CcsMessage;
import com.zealcomm.ivcs.customer.IvcsCustomerCallback;
import com.zealcomm.ivcs.customer.IvcsCustomerEvent;
import com.zealcomm.ivcs.customer.base.entity.CollectedCustomerInfo;
import com.zealcomm.ivcs.customer.base.entity.FormData;
import com.zealcomm.ivcs.customer.base.entity.IDCardAndBankInfo;
import com.zealcomm.ivcs.customer.base.entity.Service;
import com.zealcomm.ivcs.customer.base.entity.UserInfo;
import com.zealcomm.ivcs.customer.base.https.CustomerHttp;
import com.zealcomm.zms.BaseObserver;
import com.zealcomm.zms.SubSuccessData;
import com.zealcomm.zms.ZmsClient;
import com.zealcomm.zms.ZmsMessage;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.EglBase;
import org.webrtc.EglRenderer;
import org.webrtc.SurfaceViewRenderer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import irtc.base.VideoCodecParameters;
import irtc.base.VideoEncodingParameters;
import irtc.conference.Participant;
import irtc.conference.PublishOptions;
import irtc.conference.RemoteStream;

import static irtc.base.MediaCodecs.VideoCodec.H264;
import static irtc.base.MediaCodecs.VideoCodec.VP8;
import static irtc.base.MediaCodecs.VideoCodec.VP9;

public class CustomerManager extends BaseManager implements BaseObserver.BaseObserverCallback, CcsClient.CcsEventMessage, ZmsClient.ZmsEventMessage, EglRenderer.FrameListener {

    private CustomerHttp mCustomerHttp;
    private BaseObserver mBaseObserver;
    String mRoomToken, mSessionId, mSelfId;
    private IvcsCustomerCallback mIvcsCustomerCallback;
    private List<SubSuccessData> subSuccessDataList = new ArrayList<>();
    private CustomerLoginRes mLoginRes;
    private HashMap<String, SurfaceViewRenderer> renderList = new HashMap<>();
    private HashMap<String, RemoteStream> remoteStreamList = new HashMap<>();
    List<String> groupIds = new ArrayList<>();
    IvcsCustomerEvent mIvcsCustomerEvent;
    List<Service> serviceList;
    private MediaOptions mediaOptions;
    private int mScreenWidth;
    private int mScreenHeight;
    private int mStatusH;
    private PublishInterceptor publishInterceptor;
    private String baseUrl ;

    public CustomerManager(Context context) {
        mContext = context;
        TLSSocketFactory.init();
        CcsClient.getInstance().init(mContext, this);
        ZmsClient.getInstance().init(mContext, this);
        mBaseObserver = new BaseObserver(this);
        mCustomerHttp = new CustomerHttp();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);
        mScreenWidth = metrics.widthPixels;
        mScreenHeight = metrics.heightPixels;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        mStatusH = context.getResources().getDimensionPixelSize(resourceId);
    }

    public void registerEvent(IvcsCustomerEvent ivcsCustomerEvent) {
        mIvcsCustomerEvent = ivcsCustomerEvent;
    }

    public void init(String backendUrl, String userName, String password, String org, MediaOptions mediaOptions, InitCallBack initCallBack) {
        this.mediaOptions = mediaOptions;
        mInitCallback = initCallBack;
        this.baseUrl = backendUrl ;
        mCustomerHttp.init(mContext, backendUrl);
        mExecutorService = new ThreadPoolExecutor(1, 1, DURATION, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        mExecutorService.execute(() -> mCustomerHttp.login(userName, password, org, Constants.STREAM_TYPE_CUSTOMER, new ServerCallback() {
            @Override
            public void successResponse(String result) {
                mLoginRes = GsonUtil.toObject(result, CustomerLoginRes.class);
                switch (mLoginRes.getCode()) {
                    case Constants.HTTP_REQUEST_SUCCESS_CODE:
                        mName = userName;
                        mAgency = org;
                        mToken = mLoginRes.getData().getAccessToken();
                        getServiceGroups();
                        break;
                    default:
                        mInitCallback.initFailed(mLoginRes.getCode(), mLoginRes.getMessage());
                        break;
                }
            }

            @Override
            public void failedResponse(String result) {
                LoginError loginError = GsonUtil.toObject(result, LoginError.class);
                if (loginError != null) {
                    mInitCallback.initFailed(Integer.valueOf(loginError.getCode()), loginError.getMessage());
                } else {
                    mInitCallback.initFailed(HttpErrorCode.NETWORK_ERROR, HttpErrorCode.NETWORK_REQUEST_FAILED);
                }
            }
        }));

    }

    private String getGroupIdList(String groupNames) {
        String ids = null;
        for (Service item : serviceList) {
            if (groupNames.equals(item.getName())) {
                ids = item.get_id();
            }
        }
        return ids;
    }

    /**
     * call agent to join room
     */
    public void requestAgent(String videoLabel, String services, IvcsCustomerCallback customerCallback) {
        mIvcsCustomerCallback = customerCallback;
        mStreamType = videoLabel;
        mExecutorService.execute(() -> {
            SessionData sessionData = new SessionData();
            sessionData.setMedia(MEDIA);
            SessionData.ClientInfo clientInfo = new SessionData.ClientInfo();
            clientInfo.setType(CLIENT_INFO);
            sessionData.setClientInfo(clientInfo);
            SessionData.UserData userData = new SessionData.UserData();
            if (services != null) {
                userData.setService(getGroupIdList(services));
                userData.setMedia(MEDIA);
            }
            sessionData.setUserData(userData);
            JSONObject jsonObject = null;
            jsonObject = GsonUtil.toJSONObject(GsonUtil.object2String(sessionData));
            Log.i(TAG, "ccsSession data = " + jsonObject.toString());
            CcsClient.getInstance().ccsSession(jsonObject);
        });
    }

    public void joinRoom(IvcsCustomerCallback customerCallback) {
        mIvcsCustomerCallback = customerCallback;
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().joinRoom(mRoomToken);
        });
    }

    public void publish() {
        mExecutorService.execute(() -> {
            boolean hasVideoEncodingParameter = false;
            PublishOptions.Builder builder = PublishOptions.builder();
            int width = DEFAULT_PUBLISH_VIDEO_WIDTH,
                    height = DEFAULT_PUBLISH_VIDEO_HEIGHT, fps = DEFAULT_PUBLISH_VIDEO_FPS;
            if (null != publishInterceptor) {
                width = publishInterceptor.width;
                height = publishInterceptor.height;
                fps = publishInterceptor.fps;
                if (null != publishInterceptor.encodes && publishInterceptor.encodes.size() > 0) {
                    hasVideoEncodingParameter = true;
                    for (VideoEncodingParameters temp : publishInterceptor.encodes) {
                        builder.addVideoParameter(temp);
                    }
                }
            }
            if (!hasVideoEncodingParameter) {
                VideoEncodingParameters h264 = new VideoEncodingParameters(new VideoCodecParameters(H264), DEFAULT_VIDEO_MAX_BITRATE);
                VideoEncodingParameters vp8 = new VideoEncodingParameters(new VideoCodecParameters(VP8), DEFAULT_VIDEO_MAX_BITRATE);
                VideoEncodingParameters vp9 = new VideoEncodingParameters(new VideoCodecParameters(VP9), DEFAULT_VIDEO_MAX_BITRATE);
                builder.addVideoParameter(vp8)
                        .addVideoParameter(h264)
                        .addVideoParameter(vp9);
            }

            ZmsClient.getInstance().publish(
                    width,
                    height,
                    fps, mStreamType, builder.build());
        });
    }

    public void setPublishInterceptor(PublishInterceptor publishInterceptor) {
        this.publishInterceptor = publishInterceptor;
    }

    public static class PublishInterceptor {
        private int width = DEFAULT_PUBLISH_VIDEO_WIDTH,
                height = DEFAULT_PUBLISH_VIDEO_HEIGHT, fps = DEFAULT_PUBLISH_VIDEO_FPS;
        private List<VideoEncodingParameters> encodes;

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getFps() {
            return fps;
        }

        public void setFps(int fps) {
            this.fps = fps;
        }

        public List<VideoEncodingParameters> getEncodes() {
            return encodes;
        }

        public void setEncodes(List<VideoEncodingParameters> encodes) {
            this.encodes = encodes;
        }
    }

    public void exitServices(IvcsCustomerCallback customerCallback) {
        mIvcsCustomerCallback = customerCallback;
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().stop();
            CcsClient.getInstance().quitSession();
        });
    }

    public void stopShareScreen() {
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().stopScreen();
        });
    }

    public void muteLocalStreamAudio(boolean isMute) {
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().setLocalAudio(isMute);
        });
    }

    public void muteLocalStreamVideo(boolean isMute) {
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().setLocalVideo(isMute);
        });
    }

    public void snapshot(String type) {
        SurfaceViewRenderer mCustomerRenderer = renderList.get(type);
        if (mCustomerRenderer != null) {
            mCustomerRenderer.addFrameListener(this, 1.0f);
        }
    }

    public void sendText(String text, String userRole, IvcsCustomerCallback customerCallback) {
        mIvcsCustomerCallback = customerCallback;
        SessionMessage textMessage = new SessionMessage();
        textMessage.setType(CcsMessage.CCS_TEXT);
        textMessage.setFrom(mSessionId);
        textMessage.setData(text);
        textMessage.setId(new Random().nextInt() + "");
        textMessage.setTo(userRole != null ? userRole : Constants.CLIENT_AGENT);
        JSONObject message = GsonUtil.toJSONObject(GsonUtil.object2String(textMessage));
        CcsClient.getInstance().sendMessage(message, CcsMessage.CCS_TEXT);
    }

    public void sendHandWritingMessage(MessageData<HandWritingDataWrapper> message) {

        SessionMessage textMessage = new SessionMessage();
        textMessage.setType(CcsMessage.CCS_HANDWRITING);
        textMessage.setFrom(mSessionId);
        textMessage.setData(message.getData().getNameValuePairs().getData());
        textMessage.setId(message.getData().getNameValuePairs().getId());
        textMessage.setTo(Constants.CLIENT_AGENT);

        JSONObject jsonObject = GsonUtil.toJSONObject(GsonUtil.object2String(textMessage));
        CcsClient.getInstance().sendMessage(jsonObject, CcsMessage.CCS_HANDWRITING);
    }

    public void sendCoustomMessage(CustomMessage customMessage, IvcsCustomerCallback customerCallback) {
        mIvcsCustomerCallback = customerCallback;
        SessionMessage textMessage = new SessionMessage();
        textMessage.setType(CcsMessage.CCS_CUSTOM_MESSAGE);
        textMessage.setFrom(mSessionId);
        textMessage.setData(customMessage.getData());
        textMessage.setId(new Random().nextInt() + "");
        textMessage.setTo(customMessage.getReceiver() != null ? customMessage.getReceiver() : Constants.CLIENT_AGENT);
        JSONObject message = GsonUtil.toJSONObject(GsonUtil.object2String(textMessage));
        CcsClient.getInstance().sendMessage(message, CcsMessage.CCS_CUSTOM_MESSAGE);
    }

    @Override
    public void onFrame(Bitmap bitmap) {
        Log.i(TAG, "Snapshot start");
        // mLoginRes.getData().getUrls().getUploadUrl()
        // 改成 登录时 的 url
        mExecutorService.execute(() -> mCustomerHttp.uploadSignature(mToken , baseUrl, ImageUtil.saveBitmap(bitmap, "snapshot.png"), new ServerCallback() {
            @Override
            public void successResponse(String result) {
                UploadImageRes uploadImageRes = GsonUtil.toObject(result, UploadImageRes.class);
                if (uploadImageRes.getCode() == Constants.HTTP_REQUEST_SUCCESS_CODE) {
                    LinkData linkData = new LinkData();
                    linkData.setUrl(uploadImageRes.getData().getUrl());
                    linkData.setType(Constants.LINK_PIC);
                    linkData.setName(Constants.SIGNATURE);
                    SessionMessage linkMessage = new SessionMessage();
                    linkMessage.setType(CcsMessage.CCS_LINK);
                    linkMessage.setFrom(mSessionId);
                    linkMessage.setData(linkData);
                    linkMessage.setId(new Random().nextInt() + "");
                    linkMessage.setTo(Constants.CLIENT_AGENT);
                    JSONObject message = GsonUtil.toJSONObject(GsonUtil.object2String(linkMessage));
                    CcsClient.getInstance().sendMessage(message, Constants.SIGNATURE_MESSAGE);
                }
            }

            @Override
            public void failedResponse(String result) {
            }
        }));
    }

    public void sendFile(String path, String userRole, String name, String type, IvcsCustomerCallback ivcsCustomerCallback) {
        mIvcsCustomerCallback = ivcsCustomerCallback;
        // mLoginRes.getData().getUrls().getUploadUrl()
        // 这里不再获取，统一用登录的 url
        mExecutorService.execute(() -> mCustomerHttp.uploadSignature(mToken , baseUrl, path, new ServerCallback() {
            @Override
            public void successResponse(String result) {
                UploadImageRes uploadImageRes = GsonUtil.toObject(result, UploadImageRes.class);
                if (uploadImageRes.getCode() == Constants.HTTP_REQUEST_SUCCESS_CODE) {
                    LinkData linkData = new LinkData();
                    linkData.setUrl(uploadImageRes.getData().getUrl());
                    linkData.setType(type);
                    linkData.setName(name);
                    SessionMessage linkMessage = new SessionMessage();
                    linkMessage.setType(CcsMessage.CCS_LINK);
                    linkMessage.setFrom(mSessionId);
                    linkMessage.setData(linkData);
                    linkMessage.setId(new Random().nextInt() + "");
                    linkMessage.setTo(userRole != null ? userRole : Constants.CLIENT_AGENT);
                    JSONObject message = GsonUtil.toJSONObject(GsonUtil.object2String(linkMessage));
                    CcsClient.getInstance().sendMessage(message, type);
                } else {
                    mIvcsCustomerCallback.onFailed(uploadImageRes.getCode(), result);
                }
            }

            @Override
            public void failedResponse(String result) {
                mIvcsCustomerCallback.onFailed(HttpErrorCode.NETWORK_ERROR, result);
            }
        }));
    }

    public void sendLink(String url, String userRole, IvcsCustomerCallback customerCallback) {
        mIvcsCustomerCallback = customerCallback;
        LinkData linkData = new LinkData();
        linkData.setUrl(url);
        linkData.setType(CcsMessage.LINK_PAGE);
        linkData.setName(CcsMessage.LINK_PAGE);
        SessionMessage linkMessage = new SessionMessage();
        linkMessage.setType(CcsMessage.CCS_LINK);
        linkMessage.setFrom(mSessionId);
        linkMessage.setData(linkData);
        linkMessage.setId(new Random().nextInt() + "");
        linkMessage.setTo(userRole != null ? userRole : Constants.CLIENT_AGENT);
        JSONObject message = GsonUtil.toJSONObject(GsonUtil.object2String(linkMessage));
        CcsClient.getInstance().sendMessage(message, CcsMessage.CCS_LINK);
    }

    public void sendForm(String formId, @NonNull HashMap<String, String> fields, IvcsCustomerCallback ivcsCustomerCallback) {
        mIvcsCustomerCallback = ivcsCustomerCallback;
        mExecutorService.execute(() -> mCustomerHttp.commitUserInfo(mToken , formId, fields, mSessionId, new ServerCallback() {
            @Override
            public void successResponse(String result) {
                String msg = Constants.INPUT_FORM_SUCCESS;
                SessionMessage textMessage = new SessionMessage();
                textMessage.setType(CcsMessage.CCS_TEXT);
                textMessage.setFrom(mSessionId);
                textMessage.setData(msg);
                textMessage.setId(new Random().nextInt() + "");
                textMessage.setTo(Constants.CLIENT_AGENT);
                JSONObject message = GsonUtil.toJSONObject(GsonUtil.object2String(textMessage));
                CcsClient.getInstance().sendMessage(message, CcsMessage.CCS_TEXT);
            }

            @Override
            public void failedResponse(String result) {
                mIvcsCustomerCallback.onFailed(HttpErrorCode.SEND_FORM_EOORO, result);
            }
        }));
    }

    public void getServiceGroups() {

        mExecutorService.execute(() -> mCustomerHttp.getServiceGroups(mToken, new ServerCallback() {
            @Override
            public void successResponse(String result) {
                groupIds.clear();
                serviceList = GsonUtil.toObject(result, new TypeToken<List<Service>>() {
                }.getType());
                if (serviceList != null) {
                    for (Service service : serviceList) {
                        groupIds.add(service.getName());
                    }
                }
            }

            @Override
            public void failedResponse(String result) {

            }
        }));
        CcsClient.getInstance().connect(mToken, mLoginRes.getData().getUrls().getCcsurl(), null);
    }

    public EglBase.Context getEGLContext() {
        return ZmsClient.getInstance().getEGLContext();
    }

    public void registerRender(String type, SurfaceViewRenderer renderer) {
        if (type.equals(LOCAL_RENDER)) {
            ZmsClient.getInstance().setRenderer(renderer);
        }
        renderList.put(type, renderer);
        RemoteStream remoteStream = remoteStreamList.get(type);
        if (remoteStream != null && remoteStream.hasVideo()) {
            remoteStream.attach(renderer);
        }

    }

    public void unregisterRender(String type, SurfaceViewRenderer renderer) {
        renderList.remove(type);
        RemoteStream remoteStream = remoteStreamList.get(type);
        if (remoteStream != null && remoteStream.hasVideo() && renderer != null) {
            remoteStream.detach(renderer);
            renderer.removeFrameListener(this);
        }
    }

    public void shareScreen(Intent data, String type, IvcsCustomerCallback agentCallback) {
        mIvcsCustomerCallback = agentCallback;
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().screenShare(data, this.mScreenWidth / 2, (mScreenHeight + mStatusH) / 2, type);
        });
        String msg = String.format("{\"w\":%d,\"h\":%d}", mScreenWidth, mScreenHeight + mStatusH);
        SessionMessage textMessage = new SessionMessage();
        textMessage.setType(CcsMessage.CCS_SCREEN_SIZE);
        textMessage.setFrom(mSessionId);
        textMessage.setData(msg);
        textMessage.setId((new Random()).nextInt() + "");
        textMessage.setTo("agents");
        JSONObject message = GsonUtil.toJSONObject(GsonUtil.object2String(textMessage));
        CcsClient.getInstance().sendMessage(message, CcsMessage.CCS_SCREEN_SIZE);
    }

    public void readyToTalk(JSONObject userData, final String toWho) {
        ZmsClient.getInstance().setIfSubSelf(true);
        joinRoom(new IvcsCustomerCallback() {
            @Override
            public void onSuccess() {
                JSONObject message = null;
                SessionMessage statusMsg = new SessionMessage();
                statusMsg.setType(Constants.PEER_STATUS);
                statusMsg.setFrom(mSessionId);
                statusMsg.setId(new Random().nextInt() + "");
                if (toWho == null || toWho == "") {
                    statusMsg.setTo(Constants.CLIENT_AGENT);
                } else {
                    statusMsg.setTo(toWho);
                }
                StatusMsg ass = new StatusMsg();
                ass.setAt(new Date().toString());
                ass.setStatus(Constants.READY_TO_TALK);
                ass.setParticipantId(ZmsClient.getInstance().getSelfId());
                Profile profile = GsonUtil.toObject(userData.toString(), Profile.class);
                ass.setUser(profile);
                statusMsg.setData(ass);
                message = GsonUtil.toJSONObject(GsonUtil.object2String(statusMsg));
                CcsClient.getInstance().sendMessage(message, CcsMessage.CCS_READY_TO_TALK);
                // TODO 这里为：发送访客准备好会话的状态，不应该调用下面的方法，待研究，by nfl 2021.06.03.11.32
                // mIvcsCustomerEvent.onReadyTalk();
            }

            @Override
            public void onFailed(int code, String error) {
                Log.i(TAG, "joinRoom failed");
            }
        });
    }

    public void sendMessageConfirmation(JSONObject userData, final String toWho) {
        JSONObject message = null;
        SessionMessage statusMsg = new SessionMessage();
        statusMsg.setType(Constants.MESSAGE_CONFIRMATION);
        statusMsg.setFrom(mSessionId);
        statusMsg.setId(new Random().nextInt() + "");
        if (toWho == null || toWho == "") {
            statusMsg.setTo(Constants.CLIENT_AGENT);
        } else {
            statusMsg.setTo(toWho);
        }
        try {
            StatusMsg ass = new StatusMsg();
            ass.setAnswer(userData.getString("answer"));
            ass.setType(userData.getString("type"));
            if (ass.getType() == "link") {
                JSONObject data = userData.getJSONObject("data");
                LinkInfo link = new LinkInfo();
                link.setName(data.getString("name"));
                link.setType(data.getString("type"));
                link.setUrl(data.getString("url"));
                link.setUrlKey(data.getString("urlKey"));
                ass.setData(link);
            } else {
                ass.setData(userData.getString("data"));
            }
            statusMsg.setData(ass);
            message = GsonUtil.toJSONObject(GsonUtil.object2String(statusMsg));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CcsClient.getInstance().sendMessage(message, CcsMessage.CCS_MESSAGE_CONFIRMATION);
    }

    public void switchCamera() {
        ZmsClient.getInstance().switchCamera();
    }

    private void streamAdd(RemoteStream remoteStream) {
        if (remoteStream.getAttributes() != null) {
            String type = remoteStream.getAttributes().get(Constants.STREAM_TYPE);
            Log.i(TAG, "stream type =" + type);
            SurfaceViewRenderer renderer = renderList.get(remoteStream.getAttributes().get(Constants.STREAM_TYPE));
            if (renderer != null) {
                remoteStream.attach(renderer);
            }
            remoteStreamList.put(type, remoteStream);
            mIvcsCustomerEvent.onVideoLabel(type, true);
        }
    }


    @Override
    public void streamEnd(RemoteStream remoteStream) {
        String type = null;
        for (Map.Entry<String, RemoteStream> entry : remoteStreamList.entrySet()) {
            if (remoteStream.id().equals(entry.getValue().id())) {
                type = entry.getKey();
                break;
            }
        }
        if (type != null) {
            SurfaceViewRenderer renderer = renderList.get(type);
            mIvcsCustomerEvent.onVideoLabel(type, false);
            if (renderer != null) {
                remoteStreamList.get(type).detach(renderer);
                remoteStreamList.remove(type);
            }
        }
        mBaseObserver.removeStream(remoteStream);
    }

    @Override
    public void streamUpdate(RemoteStream remoteStream) {

    }

    @Override
    public void participantLeft(Participant participant) {

    }

    @Override
    public void onCcsEvent(MessageData messageData) {
        Log.i(TAG, "messageAll , CCS message =" + GsonUtil.object2String(messageData));
        JSONObject message;
        switch (messageData.getType()) {
            case CcsMessage.CCS_CONNECT_SUCCESS:
                mInitCallback.initSuccess(mToken, groupIds , null);
                break;
            case CcsMessage.CCS_CONNECT_ERROR:
                mInitCallback.initFailed(HttpErrorCode.NETWORK_ERROR, HttpErrorCode.CCS_SERVER_CONNECT_ERROR);
                break;
            case CcsMessage.CCS_LOGIN_SUCCESS:
                message = (JSONObject) messageData.getData();
                try {
                    mRoomToken = message.getString("roomToken");
                    mSessionId = message.getString("sessionId");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mIvcsCustomerCallback.onSuccess();
                break;
            case CcsMessage.CCS_LOGIN_ERROR:
                mIvcsCustomerCallback.onFailed(HttpErrorCode.CCS_LOGIN_ERROR, HttpErrorCode.CCS_LOGIN_ERROR_MESSAGE);
                break;
            case CcsMessage.CCS_PEER_STATUS:
                message = (JSONObject) messageData.getData();
                try {
                    JSONObject data = message.getJSONObject("data");
                    String status = data.getString("status");
                    Log.d(TAG, "peer-status = " + status);
                    switch (status) {
                        case CcsMessage.CCS_READY_TO_TALK:
                            mIvcsCustomerEvent.onReadyTalk();
                            joinRoom(new IvcsCustomerCallback() {
                                @Override
                                public void onSuccess() {
                                    Log.i(TAG, "joinRoom success");
                                }

                                @Override
                                public void onFailed(int code, String error) {
                                    Log.i(TAG, "joinRoom failed");
                                }
                            });
                            break;
                        case CcsMessage.CCS_HOLD_ON:
//                            String id =  message.getString("id");
//                            AckData ackData = new AckData();
//                            ackData.setId(id);
//                            AckMessage ackMessage = new AckMessage();
//                            ackMessage.setType(Constants.ACK_MESSAGE_TYPE);
//                            ackMessage.setFrom(mSessionId);
//                            ackMessage.setData(ackData);
//                            ackMessage.setId(new Random().nextInt()+"");
//                            ackMessage.setTo(Constants.CLIENT_AGENT);
//                            JSONObject msg = null;
//                            try {
//                                msg = new JSONObject(new Gson().toJson(ackMessage));
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            CcsClient.getInstance().sendMessage(msg,Constants.ACK_MESSAGE_TYPE);
                            mIvcsCustomerEvent.onTransferring();
                            break;
                        case CcsMessage.USER_JOINED:
                            try {
                                JSONObject user = data.getJSONObject("user");
                                JSONObject json_profile = user.getJSONObject("profile");
                                UserInfo userInfo = new UserInfo();
                                userInfo.setOrg(user.getString("org"));
                                userInfo.setUserId(user.getString("id"));
                                userInfo.setName(user.getString("name"));
                                userInfo.setRole(data.getString("role"));
                                Profile profile = new Profile();
                                profile.setIsRobot(json_profile.getBoolean("isRobot"));
                                userInfo.setProfile(profile);
                                mIvcsCustomerEvent.onUserJoined(userInfo);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            break;
                        case CcsMessage.USER_QUIT:
                            try {
                                JSONObject user = data.getJSONObject("user");
                                UserInfo userInfo = new UserInfo();
                                userInfo.setOrg(user.getString("org"));
                                userInfo.setUserId(user.getString("id"));
                                userInfo.setName(user.getString("name"));
                                userInfo.setRole(data.getString("role"));
                                mIvcsCustomerEvent.onUserQuit(userInfo);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            break;
                        default:
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CcsMessage.CCS_FORM:
                message = (JSONObject) messageData.getData();
                FormData formData = GsonUtil.toObject(message.toString(), FormData.class);
                if (formData.getData().getFormTemplate() != null) {
                    mIvcsCustomerEvent.onFormData(formData);
                }
                break;
            case CcsMessage.CCS_COLLECT_INFO:
                message = (JSONObject) messageData.getData();
                CollectedCustomerInfo collectedCustomerInfo = null;
                try {
                    JSONObject jsonObject = message.getJSONObject("data") ;
                    collectedCustomerInfo = GsonUtil.toObject(jsonObject.toString() , CollectedCustomerInfo.class);
                    mIvcsCustomerEvent.onCollectInfo(collectedCustomerInfo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case CcsMessage.CCS_LINK:
                message = (JSONObject) messageData.getData();
                if (message != null) {
                    try {
                        JSONObject data = message.getJSONObject("data");
                        String linkType = data.getString("type");
                        Log.i(TAG, "link type = " + linkType);
                        mIvcsCustomerEvent.onResource(data.getString("url"), linkType, data.getString("name"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case CcsMessage.CCS_TEXT:
                message = (JSONObject) messageData.getData();
                if (message != null) {
                    try {
                        String text = message.getString("data");
                        Log.i(TAG, "text = " + text);
                        mIvcsCustomerEvent.onText(text);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case CcsMessage.CCS_HANDWRITING:
                mIvcsCustomerEvent.onHandSignature();
                break;
            case CcsMessage.CCS_PEER_CMD:
                message = (JSONObject) messageData.getData();
                try {
                    JSONObject data = message.getJSONObject("data");
                    String cmd = data.getString("cmd");
                    if (CcsMessage.CCS_SCREEN_SHOT.equals(cmd)) {
//                        snapshot(LOCAL_RENDER);
                        mIvcsCustomerEvent.onScreenSnapshot();
                    } else if (CcsMessage.CCS_VIEW_SCREEN.equals(cmd)) {
                        mIvcsCustomerEvent.onViewScreen();
                    } else if (CcsMessage.CCS_RELEASE_SCREEN.equals(cmd)) {
                        stopShareScreen();
                    }else if(CcsMessage.PHOTO_GUIDEBOX.equals(cmd)){
                        mIvcsCustomerEvent.onGuideBox();
                    }
                } catch (JSONException e) {
                    LogUtil.i(ExceptionUtil.getExceptionTraceString(e));
                }
                break;
            case CcsMessage.CCS_SUCCEED_TO_SEND_MESSAGE:
                String success_type = (String) messageData.getData();
                switch (success_type) {
                    case Constants.LINK_PIC:
                    case CcsMessage.CCS_TEXT:
                    case Constants.LINK_DOC:
                        mIvcsCustomerCallback.onSuccess();
                        break;
                    default:
                        break;
                }
                break;
            case CcsMessage.CCS_FAILED_TO_SEND_MESSAGE:
                String failed_type = (String) messageData.getData();
                switch (failed_type) {
                    case Constants.LINK_PIC:
                    case Constants.LINK_DOC:
                        mIvcsCustomerCallback.onFailed(HttpErrorCode.SEND_LINK_EOORO, HttpErrorCode.SEND_LINK_ERROR_MESSAGE);
                        break;
                    case CcsMessage.CCS_TEXT:
                        mIvcsCustomerCallback.onFailed(HttpErrorCode.SEND_TEXT_EOORO, HttpErrorCode.SEND_TEXT_ERROR_MESSAGE);
                        break;
                    default:
                        break;
                }
                break;
            case CcsMessage.CCS_QUIT_SESSION:
                boolean quit = (boolean) messageData.getData();
                if (quit) {
                    mIvcsCustomerCallback.onSuccess();
                } else {
                    mIvcsCustomerCallback.onFailed(HttpErrorCode.CCS_QUIT_ERROR, HttpErrorCode.CCS_QUIT_ERROR_MESSAGE);
                }
                break;
            case CcsMessage.CCS_INVITATION_PROGRESS_MESSAGE:
                message = (JSONObject) messageData.getData();
                String status = null;
                try {
                    JSONObject data = message.getJSONObject("data");
                    status = data.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (status != null) {
                    mIvcsCustomerEvent.onInvitationProgress(status);
                }
                break;
            case CcsMessage.CCS_MARK_SCREEN:
                message = (JSONObject) messageData.getData();
                int x = -1;
                int y = -1;
                try {
                    JSONObject data = message.getJSONObject("data");
                    x = data.getInt("x");
                    y = data.getInt("y");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (x >= 0 && y >= 0) {
                    this.mIvcsCustomerEvent.onMarkScreen(x, y);
                }
                break;
            case CcsMessage.CCS_CUSTOM_MESSAGE:
                message = (JSONObject) messageData.getData();
                CustomMessage customMessage = new CustomMessage();
                try {
                    customMessage.setData(message.getString("data"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mIvcsCustomerEvent.onCustomMessage(customMessage);
            default:
                break;
        }
    }

    @Override
    public void onZmsEvent(MessageData messageData) {
        Log.i(TAG, "messageAll , zms message =" + GsonUtil.object2String(messageData));
        switch (messageData.getType()) {
            case ZmsMessage.ZMS_JOIN_SUCCESS:
                mIvcsCustomerCallback.onSuccess();
                publish();
                break;
            case ZmsMessage.ZMS_PUBLISH_SUCCESS:
                Log.e(TAG, "publish success");
                break;
            case ZmsMessage.ZMS_PUBLISH_FAILED:
                Log.e(TAG, "publishfailed");
                break;
            case ZmsMessage.ZMS_SUBSCRIBE_SUCCESS:
                SubSuccessData subSuccessData = (SubSuccessData) messageData.getData();
                subSuccessDataList.add(subSuccessData);
                streamAdd(subSuccessData.getRemoteStream());
                mBaseObserver.addRemoteStream(subSuccessData.getRemoteStream());
                break;
            default:
                break;
        }
    }

    public void submitIDCardAndBankInfo(@NonNull String token, @NonNull IDCardAndBankInfo idCardAndBankInfo, ServerCallback callback) {
        mCustomerHttp.submitIDCardAndBankInfo(token, idCardAndBankInfo, callback);
    }
}
