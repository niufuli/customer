package com.zealcomm.ivcs.customer.base.https;

import com.zealcomm.base.entity.LoginReq;
import com.zealcomm.base.entity.RecordBody;
import com.zealcomm.ivcs.customer.base.entity.FormInfo;
import com.zealcomm.ivcs.customer.base.entity.IDCardAndBankInfo;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface CustomerCommonApi {

    Call<ResponseBody> login(@Body LoginReq tokenReq);

    Call<ResponseBody> uploadSignatureImg(@Header("x-access-token") String token,@Url String url, @Part MultipartBody.Part part);

    Call<ResponseBody> getServiceGroups(@Header("x-access-token") String token);

    Call<ResponseBody> startRecord(@Header("x-access-token") String token, @Path("roomId") String roomId, @Body RecordBody recordBody);

    Call<ResponseBody> stopRecord(@Header("x-access-token") String token, @Path("roomId") String roomId, @Path("recordId") String recordId);

    Call<ResponseBody> submitIDCardAndBankInfo(@Header("x-access-token") String token, @Body IDCardAndBankInfo idCardAndBankInfo);

    Call<ResponseBody> commitForm(@Header("x-access-token") String token, @Body FormInfo formInfo , @Path("sessionId") String sessionId);

}
