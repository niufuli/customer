package com.zealcomm.ivcs.customer.base.https;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.zealcomm.base.RetrofitUtils;
import com.zealcomm.base.ServerCallback;
import com.zealcomm.base.entity.LoginReq;
import com.zealcomm.ivcs.customer.base.entity.FormInfo;
import com.zealcomm.ivcs.customer.base.entity.IDCardAndBankInfo;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerHttp {

    private String TAG = this.getClass().getName();
    public Context mContext;
    private CustomerCommonApi mCustomerApi;

    public void init(Context context, String url) {
        mContext = context.getApplicationContext();
        if (!TextUtils.isEmpty(url) && url.contains("showcase")) {
            mCustomerApi = RetrofitUtils.createApi(context, CustomerApiShowCase.class, url);
        } else {
            mCustomerApi = RetrofitUtils.createApi(context, CustomerApiDemo.class, url);
        }
    }

    public void commitUserInfo(@NonNull String token, @NonNull String formId, @NonNull HashMap<String, String> fields, @NonNull String sessionId, ServerCallback callback) {
        FormInfo body = new FormInfo();
        body.setFormTemplateId(formId);
        body.setSessionId(sessionId);
        body.setFields(fields);
        getResult(callback, mCustomerApi.commitForm(token , body , body.getSessionId()));
    }

    public void getServiceGroups(@NonNull String token, ServerCallback callback) {
        getResult(callback, mCustomerApi.getServiceGroups(token));
    }

    public void login(@NonNull String userName, @NonNull String password, @NonNull String agency, @NonNull String role, ServerCallback callback) {
        LoginReq body = new LoginReq();
        body.setUserName(userName);
        body.setPwd(password);
        body.setOrg(agency);
        body.setRole(role);
        getResult(callback, mCustomerApi.login(body));
    }

    public void uploadSignature(@NonNull String token, @NonNull String url, @NonNull String filePath, ServerCallback callback) {
        File file = new File(filePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        if (url.contains("demo")){
            getResult(callback, mCustomerApi.uploadSignatureImg(token,url + "v1/upload", body));
        }else {
            getResult(callback, mCustomerApi.uploadSignatureImg(token,url + "ivcs/api/v1/upload", body));
        }
    }

    public void getResult(final ServerCallback callback, Call<ResponseBody> call) {
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        callback.successResponse(response.body().string());
                    } else if (response.errorBody() != null) {
                        Log.e(TAG, "getResult body null");
                        callback.failedResponse(response.errorBody().string());
                    }
                } catch (Exception e) {
                    Log.e(TAG, "getResult error " + e.getMessage());
                    callback.failedResponse(e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "getResult Failure " + t.getMessage());
                callback.failedResponse(t.getMessage());
            }
        });
    }

    public void submitIDCardAndBankInfo(@NonNull String token, @NonNull IDCardAndBankInfo idCardAndBankInfo, ServerCallback callback) {
        getResult(callback, mCustomerApi.submitIDCardAndBankInfo(token, idCardAndBankInfo));
    }
}
