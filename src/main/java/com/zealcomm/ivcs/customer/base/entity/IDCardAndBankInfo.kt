package com.zealcomm.ivcs.customer.base.entity

class IDCardAndBankInfo {
    var ID: String? = null
    var address: String? = null
    var company: String? = null
    var mobile: String? = null
    var name: String? = null
    var productCategory: String? = null

    var frontPhoto: String? = "data:image/png;base64,"
    var backPhoto: String? = "data:image/png;base64,"
    var bankCardPhoto: String? = "data:image/png;base64,"
    var selfie: String? = "data:image/png;base64,"
}