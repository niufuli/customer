package com.zealcomm.ivcs.customer.base.entity;

public class SignatureData {

    private String customerId;
    private String  image;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
