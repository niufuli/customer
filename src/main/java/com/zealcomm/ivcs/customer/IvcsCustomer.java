package com.zealcomm.ivcs.customer;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.zealcomm.base.BaseManager;
import com.zealcomm.base.MessageData;
import com.zealcomm.base.ServerCallback;
import com.zealcomm.base.entity.CustomMessage;
import com.zealcomm.base.entity.GroupData;
import com.zealcomm.base.entity.HandWritingDataWrapper;
import com.zealcomm.base.entity.MediaOptions;
import com.zealcomm.ivcs.customer.base.CustomerManager;
import com.zealcomm.ivcs.customer.base.entity.IDCardAndBankInfo;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import org.webrtc.EglBase;
import org.webrtc.SurfaceViewRenderer;

import java.util.HashMap;
import java.util.List;

public class IvcsCustomer {

    private CustomerManager mCustomerManager;

    public void snapshot(@NotNull String type) {
        mCustomerManager.snapshot(type);
    }

    public interface IvcsCustomerInitCallback {
        void onSuccess(String token, List<String> groupIds);

        void onFailed(int code, String error);
    }

    /**
     * use to init SurfaceViewRenderer
     */
    public EglBase.Context getEGLContext() {
        return mCustomerManager.getEGLContext();
    }

    public IvcsCustomer(Context context) {
        mCustomerManager = new CustomerManager(context);
    }

    public void setPublishInterceptor(CustomerManager.PublishInterceptor publishInterceptor) {
        mCustomerManager.setPublishInterceptor(publishInterceptor);
    }

    /**
     * register ivcs event
     *
     * @param ivcsCustomerEvent event callback.
     */
    public void registerIvcsEvent(IvcsCustomerEvent ivcsCustomerEvent) {
        mCustomerManager.registerEvent(ivcsCustomerEvent);
    }

    /**
     * init sdk
     * set url,user info etc.
     *
     * @param accountUrl               Account system services url
     * @param name                     user name or account
     * @param password                 user password
     * @param org                      agency name
     * @param ivcsCustomerInitCallback success or failed callback.
     */
    public void init(String accountUrl, String name, String password, String org, MediaOptions mediaOptions, @NonNull IvcsCustomerInitCallback ivcsCustomerInitCallback) {
        mCustomerManager.init(accountUrl, name, password, org, mediaOptions, new BaseManager.InitCallBack() {
            @Override
            public void initSuccess(String token, List<String> gorups, List<GroupData> groupIdList) {
                ivcsCustomerInitCallback.onSuccess(token, gorups);
            }

            @Override
            public void initFailed(int errorCode, String error) {
                ivcsCustomerInitCallback.onFailed(errorCode, error);
            }
        });
    }

    /**
     * service requests
     *
     * @param videoLabel           local video stream type
     * @param serviceType          request service type(group id)
     * @param ivcsCustomerCallback success or failed callback.
     */
    public void requestSession(String videoLabel, String serviceType, @NonNull IvcsCustomerCallback ivcsCustomerCallback) {
        mCustomerManager.requestAgent(videoLabel, serviceType, ivcsCustomerCallback);
    }

    /**
     * service requests
     *
     * @param text                 text message
     * @param userRole             text message receiver
     * @param ivcsCustomerCallback success or failed callback.
     */
    public void sendText(String text, String userRole, @NonNull IvcsCustomerCallback ivcsCustomerCallback) {
        mCustomerManager.sendText(text, userRole, ivcsCustomerCallback);
    }

    public void sendHandWriting(MessageData<HandWritingDataWrapper> message) {
        mCustomerManager.sendHandWritingMessage(message);
    }

    /**
     * service requests
     *
     * @param formId               form id
     * @param fields               form data
     * @param ivcsCustomerCallback success or failed callback.
     */
    public void submitForm(String formId, @NonNull HashMap<String, String> fields, @NonNull IvcsCustomerCallback ivcsCustomerCallback) {
        mCustomerManager.sendForm(formId, fields, ivcsCustomerCallback);
    }

    /**
     * publish screen video
     *
     * @param ivcsAgentCallback success or failed callback.
     */
    public void screenShare(@NonNull Intent data, String type, @NonNull IvcsCustomerCallback ivcsAgentCallback) {
        mCustomerManager.shareScreen(data, type, ivcsAgentCallback);
    }

    /**
     * attach video stream to render view
     *
     * @param videoLabel stream video label
     * @param renderView surface view
     */
    public void attachRenderWithLabel(String videoLabel, SurfaceViewRenderer renderView) {
        mCustomerManager.registerRender(videoLabel, renderView);
    }

    /**
     * detach video stream to render view
     *
     * @param videoLabel stream video label
     * @param renderView surface view
     */
    public void detachRenderWithLabel(String videoLabel, SurfaceViewRenderer renderView) {
        mCustomerManager.unregisterRender(videoLabel, renderView);
    }

    /**
     * service exit
     */
    public void exitSession(@NonNull IvcsCustomerCallback ivcsCustomerCallback) {
        mCustomerManager.exitServices(ivcsCustomerCallback);
    }

    /**
     * send custom message to other
     *
     * @param customMessage        message content
     * @param ivcsCustomerCallback success or failed callback.
     */
    public void sendCustomMessage(CustomMessage customMessage, @NonNull IvcsCustomerCallback ivcsCustomerCallback) {
        mCustomerManager.sendCoustomMessage(customMessage, ivcsCustomerCallback);
    }

    /**
     * mute local publish stream video or audio
     *
     * @param audio audio if mute
     * @param video video if mute
     */
    public void mute(boolean audio, boolean video) {
        if (audio) {
            mCustomerManager.muteLocalStreamAudio(true);
        }
        if (video) {
            mCustomerManager.muteLocalStreamVideo(true);
        }
    }

    /**
     * unmute local publish stream video or audio
     *
     * @param audio audio if unmute
     * @param video video if unmute
     */
    public void unmute(boolean audio, boolean video) {
        if (audio) {
            mCustomerManager.muteLocalStreamAudio(false);
        }
        if (video) {
            mCustomerManager.muteLocalStreamVideo(false);
        }
    }

    /**
     * service requests
     *
     * @param path                 file path
     * @param userRole             text message receiver
     * @param ivcsCustomerCallback success or failed callback.
     */
    public void sendFile(String path, String userRole, String name, String type, @NonNull IvcsCustomerCallback ivcsCustomerCallback) {
        mCustomerManager.sendFile(path, userRole, name, type, ivcsCustomerCallback);
    }

    /**
     * service requests
     *
     * @param url                  file path
     * @param userRole             text message receiver
     * @param ivcsCustomerCallback success or failed callback.
     */
    public void sendLink(String url, String userRole, @NonNull IvcsCustomerCallback ivcsCustomerCallback) {
        mCustomerManager.sendLink(url, userRole, ivcsCustomerCallback);
    }

    /**
     * switch front camera or rear camera
     */
    public void switchCamera() {
        mCustomerManager.switchCamera();
    }

    /**
     * stop screen share stream
     */
    public void stopScreenShare() {
        mCustomerManager.stopShareScreen();
    }

    /**
     * notice agent side it's ready to talk
     */
    public void readyToTalk(JSONObject userData, String toWho) {
        mCustomerManager.readyToTalk(userData, toWho);
    }

    /**
     * notice agent side that customer received the shared resource
     */
    public void sendMessageConfirmation(JSONObject userData, String toWho) {
        mCustomerManager.sendMessageConfirmation(userData, toWho);
    }

    /**
     * 不需要加入到 SDK 中
     *
     * @param idCardAndBankInfo
     * @param callback
     */
    @Deprecated
    public void submitIDCardAndBankInfo(@NonNull IDCardAndBankInfo idCardAndBankInfo, ServerCallback callback) {
        mCustomerManager.submitIDCardAndBankInfo(mCustomerManager.mToken, idCardAndBankInfo, callback);
    }

}
