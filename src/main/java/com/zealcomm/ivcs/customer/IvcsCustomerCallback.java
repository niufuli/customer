package com.zealcomm.ivcs.customer;

public interface IvcsCustomerCallback {

    void onSuccess();
    void onFailed(int code,String error);

}
